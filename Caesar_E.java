package Cipher;

import java.io.DataInputStream;

/**
 *
 * @author Ridims
 */
public class Caesar_E {
    public static void main(String[] args){
        DataInputStream input = new DataInputStream(System.in);
        String k, hs = "", h_reverse = "", h_final="";
        //String h_ascii, h_sub="";
        int n, length = 0, ascii, n_final = 0;
        
        try {
            System.out.println("Masukkan kata yang akan di enkripsi : ");
            k = input.readLine().replaceAll("\\s", "").toLowerCase();
            System.out.println("Geser berapa huruf ? : " );
            n = Integer.parseInt(input.readLine());
            
            //part 1 : caesar cipher -------------------------------------------
            for(char c : k.toCharArray())////untuk c <= k
                /*
                K = ABC
                c -> a -> b -> c
                */
                hs += Character.toString((char)(((c-'a'+ n)%26)+'a'));
            //System.out.println("Hasil enkripsi sementara : "+ hs);
            
            //part 2 : reverse full --------------------------------------------
            length = hs.length();
            for(int i = length -1; i>=0;i--){h_reverse = h_reverse + hs.charAt(i);}
            //System.out.println("Hasil enkripsi setelah reverse : " + h_reverse);
            
            //part 3 : caesar cipher -------------------------------------------
            for(char c : h_reverse.toCharArray())
                h_final += Character.toString((char)(((c-'a' + n)%26)+'a'));
                        /*.replaceFirst("a", "@").replaceFirst("i", "!")
                        .replaceFirst("u", "U").replaceFirst("e","3")
                        .replaceFirst("o", "Q")*/
            System.out.println("Hasil enkripsi akhir : " + h_final);
            
        } catch (Exception e) {
            System.out.println("Error : " +e);
        }
        
        /*
        char c = 'a';
        c = (char) (((c - 'a' + n) % 26) + 'a');
        
        I'll try explaining what is happening here. All the characters that a char type can hold has an ASCII code of integer type.
        c = (char) (((c - 'a' + n) % 26) + 'a');
        
        the value of c is 'a' in the above code. The ASCII value of 'a' is 97. So the expression can be re-written as
        c = (char) (((a - a + n) % 26) +' a');
        c = (char) (((97 - 97 + 1) % 26) + 97);
        c = (char) (((1) % 26) + 97);
        c = (char) ((1 + 97);
        which results in
        c = (char) (98)
        
        System.out.println(c);*/
    }
}
