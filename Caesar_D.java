package Cipher;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author Ridims
 */
public class Caesar_D {
    public static void main(String[] args) throws IOException{
        DataInputStream input = new DataInputStream(System.in);
        String k, hs = "", h_reverse="", h_final="", h_bruteForce="";
        int n, length=0, i;
        
        System.out.println("Masukkan kata yang akan di dekrip : ");
        k = input.readLine().toLowerCase();
        
        //for(;;){
        try {
            System.out.println("Geser berapa huruf?");
            n = Integer.parseInt(input.readLine());
            
            //part1 : caesar jrxucurrkncipher ----------------------------------
            for(char c:k.toCharArray()) // tanda " : " artinya for-each
                hs += Character.toString((char)((((c -'a'- n)+26)%26)+'a'));
            //System.out.println("Hasil dekrip sementara : "+hs);
            
            //part 2 : reverse full -----------------------------------
            length = hs.length();
            for(i = length-1;i>=0;i--)
                h_reverse = h_reverse + hs.charAt(i);
            //System.out.println("Hasil dekrip setelah reverse : " + h_reverse);
            
            //part 3 : caesar cipher ----------------------------------
            for(char c : h_reverse.toCharArray())
                h_final += Character.toString((char)((((c -'a'- n)+26)%26)+'a'));
            System.out.println("Hasil dekrip akhir : " + h_final);
            hs="";
            h_reverse="";
            h_final="";
        } catch (Exception e) {
            e.printStackTrace();
        //}
        
        /*
        */
        }
    }
}
